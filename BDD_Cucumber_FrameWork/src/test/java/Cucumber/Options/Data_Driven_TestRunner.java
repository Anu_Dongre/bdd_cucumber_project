package Cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Data_Driven_Features", glue = { "Data_Driven_step.Definitions" }, tags="@Patch_API_TestCases or"
		+ " @Post_API_TestCases or @Put_API_TestCases"
		+ "")

public class Data_Driven_TestRunner {
	

}  
