package Request_Repository;

import org.testng.annotations.DataProvider;

public class TestNG_Data_Provider {
	@DataProvider()
	public Object[][] post_data_provider() {
		return new Object[][] {

				{ "Anuradha", "QA" },
				{ "Ishika", "SrQA" }, 
				{ "Saniya", "Mgr" }
		};
	
	}
}
